/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS
 */
public class Pruebas20200207 {
    
    public Pruebas20200207() {
    }
    
    @Test
    public void testHello(){
        assertTrue(true);
    }
    @Test
    public void testRompecabezasCuadrado(){
        int [][] tablero = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,0}};
        
        Rompecabezas rompecabezas = new Rompecabezas(tablero);
        boolean respuesta = rompecabezas.verificarMatrizCuadrada();
        assertTrue(respuesta);  
    }
    
    @Test
    public void testRompecabezasTieneEspacioDeslizante(){
        int [][] tablero = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,0}};
        
        Rompecabezas rompecabezas = new Rompecabezas(tablero);
        boolean respuesta = rompecabezas.verificarHueco();
        assertTrue(respuesta);
    }
    @Test
    public void testRompecabezasNoTieneEspacioDeslizante(){
        int [][] tablero = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
        
        Rompecabezas rompecabezas = new Rompecabezas(tablero);
        boolean respuesta = rompecabezas.verificarHueco();
        assertFalse(respuesta);
    }
     @Test
    public void testRompecabezasImprimeMatriz(){
        
        int [][] tablero = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,0}};
        String esperado = "1         2         3         4         \n5         6         7         8         \n9         10        11        12        \n13        14        15        0         \n";
        Rompecabezas rompecabezas = new Rompecabezas(tablero);
        String imprimible = rompecabezas.generarImprimible();
        assertEquals(esperado, imprimible);
         System.out.println(imprimible);
    }
    
    @Test
    public void testPosiblesMovidas(){
        int [][] tablero = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,0}};
        Rompecabezas rompecabezas = new Rompecabezas(tablero);
        ArrayList<int[]> listaEsperada = new ArrayList<int[]>();
        int[] posicion1 = {2,3};
        int[] posicion2 = {3,2};
        listaEsperada.add(posicion1);
        listaEsperada.add(posicion2);
        
        ArrayList<int[]> listaObtenida = rompecabezas.posiblesMovidas(3, 3);
        assertArrayEquals(listaEsperada.toArray(), listaObtenida.toArray());
    }
    @Test
    public void testAplicarMovida(){
        System.out.println("Prueba de movida");
        int [][] tablero = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,0}};
        Rompecabezas rompecabezas = new Rompecabezas(tablero);
        ArrayList<int[]> listaPosiblesMovidas = rompecabezas.posiblesMovidas(3, 3);
        String imprimible = rompecabezas.generarImprimible();
        System.out.println(imprimible);
        int[][] esperadoPosible1 = {{1,2,3,4},
                                   {5,6,7,8},
                                   {9,10,11,0},
                                   {13,14,15,12}};
        int[][] esperadoPosible2 = {{1,2,3,4},
                                   {5,6,7,8},
                                   {9,10,11,12},
                                   {13,14,0,15}};
        
        
        int[] posicionHueco = {3,3};
        int[][] movido = rompecabezas.aplicarMovida(listaPosiblesMovidas, posicionHueco);
        Rompecabezas rompecabezasMovido = new Rompecabezas(movido);
        imprimible = rompecabezasMovido.generarImprimible();
        if(movido==esperadoPosible1){
            assertTrue(true);
        }else{
        if(movido==esperadoPosible2){
            assertTrue(true);
          }
        }
        System.out.println(imprimible);
    }
}
