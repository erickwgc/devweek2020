
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS
 */
public class Rompecabezas {
     int [][] matriz;
    public Rompecabezas(int [][] matriz) {
        this.matriz=matriz;
    }
   
    public  boolean verificarMatrizCuadrada(){
        boolean res=true;
        int n=matriz.length;
        for (int i = 0; i < matriz.length; i++) {
          if (n!=matriz[i].length) {
            res=false;
            }  
        }
        
        return res;
    }
    public boolean verificarHueco(){
        boolean existe= false;
        int cantidad=0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (matriz[i][j]==0) {
                    cantidad++;
                }
            }
        }
        if (cantidad==1) {
            existe=true;
        }
        return existe;
    }
    public String generarImprimible(){
        String resultado ="";
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                resultado = resultado + matriz[i][j];
                    resultado+=generarEspaciosEnBlanco(matriz[i][j]+"");
            }
            resultado+="\n";
        }
        return resultado;
    }
    public String generarEspaciosEnBlanco(String cadena){
        int tamano = cadena.length();
        String espaciosGenerados="";
        String espacio=" ";
        while(tamano<10){
            espaciosGenerados+=espacio;
            tamano++;
        }
        return espaciosGenerados;
    }
    public ArrayList<int[]> posiblesMovidas(int fila, int columna){
        int[] movidaArriba=new int[2];
        int[] movidaDerecha=new int[2];
        int[] movidaAbajo=new int[2];
        int[] movidaIzquierda=new int[2];
        
        ArrayList<int[]> listaPosiblesMovidas = new ArrayList<int[]>();
        if(fila>=0 && fila <matriz.length && columna >=0 && columna <matriz.length){
            //movidaArriba
            if((fila-1) >=0){
                movidaArriba[0] = fila-1;
                movidaArriba[1] = columna;
                listaPosiblesMovidas.add(movidaArriba);
            }
            //movidaDerecha
            if((columna+1)<matriz.length){
                movidaDerecha[0] = fila;
                movidaDerecha[1] = columna+1;
                listaPosiblesMovidas.add(movidaDerecha);
            }
            //movidaAbajo
            if((fila+1)<matriz.length){
                movidaAbajo[0] = fila+1;
                movidaAbajo[1] = columna;
                listaPosiblesMovidas.add(movidaAbajo);
            }
            //movidaIzquierda
            if((columna-1)>=0){
                movidaIzquierda[0] = fila;
                movidaIzquierda[1] = columna-1;
                listaPosiblesMovidas.add(movidaIzquierda);
            }
        } 
        return listaPosiblesMovidas;
    }
    public int[][] aplicarMovida(ArrayList<int[]> listaPosiblesMovidas, int[] posicion){
        int[] movidaARealizar = listaPosiblesMovidas.get((int)(Math.random()*listaPosiblesMovidas.size()));
        int elementoAux = matriz[movidaARealizar[0]][movidaARealizar[1]];
        matriz[movidaARealizar[0]][movidaARealizar[1]] = 0;
        matriz[posicion[0]][posicion[1]] = elementoAux;
        return matriz;
    }
    
}
